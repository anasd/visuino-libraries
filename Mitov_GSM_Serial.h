////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//     This software is supplied under the terms of a license agreement or    //
//     nondisclosure agreement with Mitov Software and may not be copied      //
//     or disclosed except in accordance with the terms of that agreement.    //
//         Copyright(c) 2002-2016 Mitov Software. All Rights Reserved.        //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#ifndef _MITOV_GSM_SERIAL_h
#define _MITOV_GSM_SERIAL_h

#include <Mitov.h>

namespace Mitov
{
//---------------------------------------------------------------------------
	class MitovGSMSerial;
//---------------------------------------------------------------------------
	class MitovGSMSerialBasicFunction : public OpenWire::Component
	{
	protected:
		MitovGSMSerial &FOwner;

	public:
		virtual void SystemStart() {}
		virtual bool TryProcessRequestedInput( String ALine, bool &ALockInput, bool &AResponseCompleted ) { return false; }
		virtual bool TryProcessInput( String ALine, bool &ALockInput ) { return false; }

	public:
		MitovGSMSerialBasicFunction( MitovGSMSerial &AOwner );
	};
//---------------------------------------------------------------------------
	class MitovGSMSerial : public OpenWire::Component
	{
		typedef OpenWire::Component inherited;

	protected:
		Mitov::SimpleList<MitovGSMSerialBasicFunction *>	FFunctions;
		Mitov::SimpleList<MitovGSMSerialBasicFunction *>	FResponseHandlersQueue;

	public:
		void AddFunction( MitovGSMSerialBasicFunction *AFunction )
		{
			FFunctions.push_back( AFunction );
		}

	public:
		void SendQuery( String AQuery )
		{
			FStream.print( "AT+" );
			FStream.print( AQuery );
			FStream.println( "?" );
		}

		void SendQueryRegisterResponse( MitovGSMSerialBasicFunction *ASender, String AQuery )
		{
			SendQuery( AQuery );
			FResponseHandlersQueue.push_back( ASender );
		}

	public:
		Stream &FStream;

	protected:
		char		FBuffer[ 256 ];
		uint8_t		FIndex = 0;
		uint16_t	FLockInputIndex = 0;
		uint16_t	FLockRequestedInputIndex = 0;

	protected:
		void ReadSerial()
		{
			int AChar = FStream.read();
//			Serial.print( AChar );
			if( AChar < 0 )
				return;

//			Serial.print( (char)AChar );
//			if( AChar < ' ' )
//				Serial.println( AChar );

			if( AChar == 13 )
				return;

			if( AChar != 10 )
			{
				FBuffer[ FIndex ++ ] = AChar;
				if( FIndex < 255 )
					return;
			}

//			Serial.println( "TEST!!!" );
//			Serial.println( "" );
//			Serial.println( FIndex );

			FBuffer[ FIndex ] = '\0';
			FIndex = 0;

			String AString = FBuffer;

//			Serial.println( AString );

			bool	ALockInput;
			bool	AResponseCompleted = false;
//			Serial.print( "FLockInputIndex : " ); Serial.println( FLockInputIndex );
			if( FLockRequestedInputIndex )
			{
				ALockInput = true;
				if( FResponseHandlersQueue[ FLockRequestedInputIndex - 1 ]->TryProcessRequestedInput( AString, ALockInput, AResponseCompleted ))
				{
					if( AResponseCompleted )
						FResponseHandlersQueue.Delete( FLockInputIndex - 1 );

					if( ! ALockInput )
						FLockInputIndex = 0;
				}

				return;
			}

			ALockInput = false;
			AResponseCompleted = false;
			for( int i = 0; i < FResponseHandlersQueue.size(); ++i )
				if( FResponseHandlersQueue[ i ]->TryProcessRequestedInput( AString, ALockInput, AResponseCompleted ))
				{
					if( ALockInput )
						FLockRequestedInputIndex = i + 1;

					if( AResponseCompleted )
						FResponseHandlersQueue.Delete( i );

					return;
				}

			if( FLockInputIndex )
			{
//				Serial.println( "FLockInputIndex" );
				ALockInput = true;
				FFunctions[ FLockInputIndex - 1 ]->TryProcessInput( AString, ALockInput );
				if( ! ALockInput )
					FLockInputIndex = 0;

				return;
			}

//			Serial.println( "*****" );
			ALockInput = false;
			for( int i = 0; i < FFunctions.size(); ++i )
				if( FFunctions[ i ]->TryProcessInput( AString, ALockInput ))
				{
					if( ALockInput )
						FLockInputIndex = i + 1;

					return;
				}
/*
			ALine.trim();
			if( ALine == "RDY" )
			{
				return true;
			}

			if( ALine == "NORMAL POWER DOWN" )
			{
				return true;
			}

			if( ALine == "Call Ready" )
			{
			}

*/
		}

	protected:
		virtual void SystemLoopBegin( unsigned long currentMicros ) override
		{
			inherited::SystemLoopBegin( currentMicros );
			ReadSerial();
		}

		virtual void SystemStart() override
		{
			for( int i = 0; i < FFunctions.size(); ++i )
				FFunctions[ i ]->SystemStart();
		}

	public:
		MitovGSMSerial( Mitov::BasicSerialPort &ASerial ) :
			FStream( ASerial.GetStream() )
		{
		}

	};
//---------------------------------------------------------------------------
	class MitovGSMSerialSMSSendMessageFunction : public MitovGSMSerialBasicFunction, public ClockingSupport
	{
		typedef MitovGSMSerialBasicFunction inherited;

	public:
		OpenWire::SourcePin	ReferenceOutputPin;

	public:
		String Address;
		String Text;

	public:
		void SetText( String AValue )
		{
			if( Text == AValue )
				return;

			Text = AValue;

			if( ! ClockInputPin.IsConnected() )
				TrySendValue();

		}

	protected:
		bool FLocked = false;

	protected:
		void TrySendValue()
		{
			if( Address == "" )
				return;

			if( Text == "" )
				return;

//			Serial.println( "TEST555" );

			FOwner.SendQueryRegisterResponse( this, "AT + CMGS = \"" + Address + "\"" );
			FOwner.FStream.println( Text );
			FOwner.FStream.println((char)26);	//the ASCII code of the ctrl+z is 26
		}

	public:
		virtual bool TryProcessRequestedInput( String ALine, bool &ALockInput, bool &AResponseCompleted ) override
		{
			if( FLocked )
			{
				ALine.trim();
				if( ALine == "OK" )
				{
//					Serial.println( "ALockInput = false" );
					AResponseCompleted = true;
					ALockInput = false;
					FLocked = false;
				}

				return true;
			}

			if( ALine.startsWith( "+CMGS:" ) )
			{
				ALine.remove( 0, 6 );
				ALine.trim();
				uint32_t AReference = ALine.toInt();

				ReferenceOutputPin.Notify( &AReference );

				ALockInput = true;
				FLocked = true;
				return true;
			}

			return false;
		}

	protected:
		virtual void DoClockReceive( void *_Data ) override
		{
			TrySendValue();
		}

		void DoInputChange( void *_Data )
		{
			if( ClockInputPin.IsConnected() )
				return;

			TrySendValue();
		}

	public:
		using inherited::inherited;

	};
//---------------------------------------------------------------------------
	class MitovGSMSerialBasicExpectOKFunction : public MitovGSMSerialBasicFunction
	{
		typedef MitovGSMSerialBasicFunction inherited;

	public:
		virtual bool TryProcessRequestedInput( String ALine, bool &ALockInput, bool &AResponseCompleted ) override
		{
			ALine.trim();
			if( ALine == "OK" )
			{
//					Serial.println( "ALockInput = false" );
				AResponseCompleted = true;
				ALockInput = false;
				return true;
			}

			return false;
		}

	public:
		using inherited::inherited;

	};
//---------------------------------------------------------------------------
	class MitovGSMSerialSMSSetModeFunction : public MitovGSMSerialBasicExpectOKFunction, public ClockingSupport
	{
		typedef MitovGSMSerialBasicExpectOKFunction inherited;

	public:
		bool	PDUMode = false;

	protected:
		virtual void DoClockReceive( void *_Data ) override
		{
			FOwner.SendQueryRegisterResponse( this, String( "AT+CMGF=" ) + ( PDUMode ) ? "0" : "1" );
		}

	public:
		virtual void SystemStart() override 
		{
//			inherited::SystemStart();
			if( ! ClockInputPin.IsConnected() )
				DoClockReceive( nullptr );

		}

	public:
		using inherited::inherited;

	};
//---------------------------------------------------------------------------
	class MitovGSMSerialSMSGetModeFunction : public MitovGSMSerialBasicFunction, public ClockingSupport
	{
		typedef MitovGSMSerialBasicFunction inherited;

	public:
		OpenWire::SourcePin	InPDUModeOutputPin;

	protected:
		bool FLocked = false;

	protected:
		virtual void DoClockReceive( void *_Data ) override
		{
			FOwner.SendQueryRegisterResponse( this, "AT+CMGF?" );
		}

	public:
		virtual bool TryProcessRequestedInput( String ALine, bool &ALockInput, bool &AResponseCompleted ) override
		{
			if( FLocked )
			{
				ALine.trim();
				if( ALine == "OK" )
				{
//					Serial.println( "ALockInput = false" );
					AResponseCompleted = true;
					ALockInput = false;
					FLocked = false;
				}

				return true;
			}

			if( ALine.startsWith( "+CMGF:" ) )
			{
				ALine.remove( 0, 6 );
				ALine.trim();
				uint8_t AReference = ALine.toInt();

				InPDUModeOutputPin.SendValue<bool>( AReference == 0 );

				ALockInput = true;
				FLocked = true;
				return true;
			}

			return false;
		}

	public:
		virtual void SystemStart() override
		{
//			inherited::SystemStart();
			if( ! ClockInputPin.IsConnected() )
				DoClockReceive( nullptr );

		}

	public:
		using inherited::inherited;

	};
//---------------------------------------------------------------------------
	class MitovGSMSerialSMSBasicMessageFunction : public MitovGSMSerialBasicFunction
	{
		typedef MitovGSMSerialBasicFunction inherited;

	protected:
		bool FLocked = false;

	protected:
		virtual void ProcessLine( String ALine, bool AIsSecondLine ) {}

	public:
		virtual bool TryProcessInput( String ALine, bool &ALockInput ) override
		{
			if( FLocked )
			{
//				Serial.println( ALine );
				ProcessLine( ALine, true );
				ALockInput = false;
				FLocked = false;

				return true;
			}

			ALine.trim();
//			Serial.println( "TEST3333" );
			if( ALine.startsWith( "+CMT:" )) //"RDY" )
			{
//				Serial.println( "TEST111" );
				ProcessLine( ALine, false );

				FLocked = true;
				ALockInput = true;
				return true;
			}

			return false;
		}

	public:
		using inherited::inherited;

	};
//---------------------------------------------------------------------------
	class MitovGSMSerialSMSMessageFunction : public MitovGSMSerialSMSBasicMessageFunction
	{
		typedef MitovGSMSerialSMSBasicMessageFunction inherited;

	public:
		OpenWire::SourcePin	OutputPin;
		OpenWire::SourcePin	AddressOutputPin;
		OpenWire::SourcePin	NameOutputPin;
		OpenWire::SourcePin	TimeOutputPin;
		OpenWire::SourcePin	TimeZoneOutputPin;

	protected:
		bool FIsPDU = false;

	protected:
		bool ExtractTimeStamp( String ATimeStamp, TDateTime &ADateTime, int32_t &ATimeZone )
		{
			if( ATimeStamp.length() < 17 )
				return false;

			String AText = ATimeStamp.substring( 0, 2 );
			int16_t AYear = 2000 + AText.toInt();

			AText = ATimeStamp.substring( 3, 5 );
			int16_t AMonth = AText.toInt();

			AText = ATimeStamp.substring( 6, 8 );
			int16_t ADay = AText.toInt();

			AText = ATimeStamp.substring( 9, 11 );
			int16_t AHour = AText.toInt();

			AText = ATimeStamp.substring( 12, 14 );
			int16_t AMinute = AText.toInt();

			AText = ATimeStamp.substring( 15, 17 );
			int16_t ASecond = AText.toInt();

			AText = ATimeStamp.substring( 17, 20 );
			ATimeZone = AText.toInt();

			return ADateTime.TryEncodeDateTime( AYear, AMonth, ADay, AHour, AMinute, ASecond, 0 );
		}

	public:
		virtual void ProcessLine( String ALine, bool AIsSecondLine ) override
		{
			if( AIsSecondLine )
			{
				if( FIsPDU )
				{
//					Serial.println( ALine );
					// DODO: Decode!
					// http://soft.laogu.com/download/sms_pdu-mode.pdf
					// https://www.diafaan.com/sms-tutorials/gsm-modem-tutorial/online-sms-submit-pdu-decoder/
					// http://jazi.staff.ugm.ac.id/Mobile%20and%20Wireless%20Documents/s_gsm0705pdu.pdf
				}

				else
					OutputPin.SendValue( ALine );
			}

			else
			{
				FIsPDU = false;
				ALine.remove( 0, 5 );
				ALine.trim();
				String AAddressOrNameOrLength;
				if( Func::ExtractOptionallyQuotedCommaText( ALine, AAddressOrNameOrLength ))
				{
//					Serial.println( "TTT1" );
//					Serial.println( AAddressOrName );
					String ANameOrLength;
					if( Func::ExtractOptionallyQuotedCommaText( ALine, ANameOrLength ))
					{
//						Serial.println( "TTT2" );
						String ATimeStamp;
						if( Func::ExtractOptionallyQuotedCommaText( ALine, ATimeStamp ))
						{ 
							// Text Mode
							AddressOutputPin.SendValue( AAddressOrNameOrLength );
							NameOutputPin.SendValue( ANameOrLength );

							Mitov::TDateTime ADateTime;
							int32_t ATimeZone;
							if( ExtractTimeStamp( ATimeStamp, ADateTime, ATimeZone ))
							{
								TimeOutputPin.Notify( &ADateTime );
								TimeZoneOutputPin.Notify( &ATimeZone );
							}
						}

						else 
						{
//							Serial.println( "YYYYYYYYY" );
							FIsPDU = true;
//							int ALength = ANameOrLength.toInt();
							NameOutputPin.SendValue( AAddressOrNameOrLength );
						}
					}

					else
					{
//						Serial.println( "YYYYYYYYY" );
						FIsPDU = true;
	//					int ALength = ANameOrLength.toInt();
						NameOutputPin.Notify( (void *)"" );
					}
				}
			}
		}

	public:
		using inherited::inherited;

	};
//---------------------------------------------------------------------------
	class MitovArduinoGSMSerialDetectDefinedTextFunction : public MitovGSMSerialBasicFunction
	{
		typedef MitovGSMSerialBasicFunction inherited;

	public:
		OpenWire::SourcePin	OutputPin;

	protected:
		const char *FText;

	public:
		virtual bool TryProcessInput( String ALine, bool &ALockInput ) override
		{
			ALine.trim();
			if( ALine == FText ) //"RDY" )
			{
				OutputPin.Notify( nullptr );
				return true;
			}

			return false;
		}

	public:
		MitovArduinoGSMSerialDetectDefinedTextFunction(  MitovGSMSerial &AOwner, const char *AText ) : 
			inherited( AOwner ),
			FText( AText )
		{
		}

	};
//---------------------------------------------------------------------------
	class MitovGSMSerialSMSMessageReceivedFunction : public MitovGSMSerialBasicFunction
	{
		typedef MitovGSMSerialBasicFunction inherited;

	public:
		OpenWire::SourcePin	StorageOutputPin;
		OpenWire::SourcePin	IndexOutputPin;
		OpenWire::SourcePin	ReceivedOutputPin;

	public:
		virtual bool TryProcessInput( String ALine, bool &ALockInput ) override
		{
			ALine.trim();
			if( ALine.startsWith( "+CMTI:" ) )
			{
//				Serial.println( "ALine.startsWith" );
//				Serial.println( ALine );
				String AStorageType;
				if( Func::ExtractOptionallyQuotedCommaText( ALine, AStorageType ))
				{
					String AIndexText;
					if( Func::ExtractOptionallyQuotedCommaText( ALine, AIndexText ))
					{
						StorageOutputPin.SendValue( AStorageType );

						int32_t	AIndex = AIndexText.toInt();
						IndexOutputPin.Notify( &AIndex );

						ReceivedOutputPin.Notify( nullptr );
					}
				}

				return true;
			}

			return false;
		}

	public:
		using inherited::inherited;

	};
//---------------------------------------------------------------------------
/*
	class MitovSIM900DetectReadyFunction : public MitovGSMSerialBasicFunction
	{
	public:
		OpenWire::SourcePin	OutputPin;

	public:
		virtual bool TryProcessInput( String ALine, bool &ALockInput ) override
		{
			ALine.trim();
			if( ALine == "Call Ready" )
			{
				OutputPin.Notify( nullptr );
				return true;
			}

			return false;
		}

	};
//---------------------------------------------------------------------------
	class MitovSIM900DetectReadyFunction : public MitovGSMSerialBasicFunction
	{
	public:
		OpenWire::SourcePin	OutputPin;

	public:
		virtual bool TryProcessInput( String ALine, bool &ALockInput ) override
		{
			ALine.trim();
			if( ALine == "NORMAL POWER DOWN" )
			{
				OutputPin.Notify( nullptr );
				return true;
			}

			return false;
		}

	};
*/
//---------------------------------------------------------------------------
	class MitovSIM900ReadADCFunction : public MitovGSMSerialBasicFunction, public ClockingSupport
	{
		typedef MitovGSMSerialBasicFunction inherited;

	public:
		OpenWire::TypedSourcePin<float> OutputPin;
		OpenWire::TypedSourcePin<bool>	ErrorOutputPin;

	protected:
		bool FLocked : 1;
		bool FStarted : 1;
		bool FErrorStarted : 1;

	protected:
		virtual void DoClockReceive( void *_Data ) override
		{
			FOwner.SendQueryRegisterResponse( this, "CADC" );
		}

	public:
		virtual bool TryProcessRequestedInput( String ALine, bool &ALockInput, bool &AResponseCompleted ) override
		{
			if( FLocked )
			{
				ALine.trim();
				if( ALine == "OK" )
				{
//					Serial.println( "ALockInput = false" );
					AResponseCompleted = true;
					ALockInput = false;
					FLocked = false;
				}

				return true;
			}

			if( ALine.startsWith( "+CADC:" ) )
			{
//				Serial.println( "ALine.startsWith" );
//				Serial.println( ALine );

				int APos = ALine.indexOf( ",", 6 );
				if( APos >= 0 )
				{
					String ALeft = ALine.substring( 6, APos );
					String ARight = ALine.substring( APos + 1 );
					ALeft.trim();
					ARight.trim();
					int ASuccess = ALeft.toInt();
					float AValue = ARight.toInt();

//					Serial.println( ASuccess );
//					Serial.println( AValue );

					ErrorOutputPin.SetValue( ASuccess != 0, FErrorStarted );
					FErrorStarted = true;

					if( ASuccess )
					{
						AValue /= 2800;
						OutputPin.SetValue( AValue, FStarted );
						FStarted = true;
					}
				}

				ALockInput = true;
				FLocked = true;
				return true;
			}

			return false;
		}

	public:
		MitovSIM900ReadADCFunction( MitovGSMSerial &AOwner ) :
			inherited( AOwner ),
			FLocked( false ),
			FStarted( false ),
			FErrorStarted( false )
		{
		}

	};
//---------------------------------------------------------------------------
	MitovGSMSerialBasicFunction::MitovGSMSerialBasicFunction( MitovGSMSerial &AOwner ) :
		FOwner( AOwner )
	{
		FOwner.AddFunction( this );
	}
//---------------------------------------------------------------------------
	}

#endif
